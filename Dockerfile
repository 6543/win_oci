FROM --platform=$BUILDPLATFORM golang AS build

WORKDIR /src
COPY . .
ARG TARGETOS TARGETARCH
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    go build -o command main.go

FROM scratch
COPY --from=build /src/command /command

ENTRYPOINT ["/command"]
